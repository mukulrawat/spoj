# -*- coding: utf-8 -*-
"""
Created on Sun Oct 12 10:18:30 2014

SPOJ Problem: Adding Reversed Numbers

@author: mukul
"""

n = int(raw_input())
while n > 0:
    x, y = raw_input().split()
    print int(str(int(x[::-1]) + int(y[::-1]))[::-1])
    n -= 1
