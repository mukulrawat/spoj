# -*- coding: utf-8 -*-
"""
Created on Sun Oct 12 13:17:01 2014

SPOJ problem: Factorial

@author: mukul
"""

t = int(raw_input())
while t > 0:
    n = int(raw_input())
    x = 5
    count = 0
    while True:
        res = n/x
        if res == 0:
            break
        count += res
        x *= 5
    print count
    t-=1

