# -*- coding: utf-8 -*-
"""
Created on Sun Oct 12 16:07:03 2014

SPOJ Problem: Transform the Expression

@author: mukul
"""

t = int(raw_input())
while t > 0:
    exp = raw_input()
    # create a stack to hold the expression
    stack = []
    # variable to store results
    res = ""
    # insert expression in the stack
    for e in exp:
        if e != ')':
            stack.append(e)
        else:
            a = stack.pop()
            b = stack.pop()
            c = stack.pop()
            stack.pop()
            res = (b+a+c)
            # print res
            stack.append(res)
            res = ""
    print stack[0][::-1]
    t -= 1

